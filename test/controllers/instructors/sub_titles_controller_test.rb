require "test_helper"

class Instructors::SubTitlesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get instructors_sub_titles_new_url
    assert_response :success
  end

  test "should get show" do
    get instructors_sub_titles_show_url
    assert_response :success
  end

  test "should get create" do
    get instructors_sub_titles_create_url
    assert_response :success
  end

  test "should get edit" do
    get instructors_sub_titles_edit_url
    assert_response :success
  end

  test "should get update" do
    get instructors_sub_titles_update_url
    assert_response :success
  end

  test "should get destroy" do
    get instructors_sub_titles_destroy_url
    assert_response :success
  end
end
