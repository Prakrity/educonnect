require "test_helper"

class Instructors::ImagesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get instructors_images_new_url
    assert_response :success
  end

  test "should get show" do
    get instructors_images_show_url
    assert_response :success
  end

  test "should get create" do
    get instructors_images_create_url
    assert_response :success
  end

  test "should get edit" do
    get instructors_images_edit_url
    assert_response :success
  end

  test "should get update" do
    get instructors_images_update_url
    assert_response :success
  end

  test "should get destroy" do
    get instructors_images_destroy_url
    assert_response :success
  end
end
