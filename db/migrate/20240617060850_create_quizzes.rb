class CreateQuizzes < ActiveRecord::Migration[7.1]
  def change
    create_table :quizzes do |t|
      t.references :lesson, null: false, foreign_key: true
      t.references :instructor, foreign_key: { to_table: :users }, null: false
      t.string :question
      t.string :answer1
      t.string :answer2
      t.string :answer3
      t.string :answer4
      t.string :right_answer
      t.string :level

      t.timestamps
    end
  end
end
