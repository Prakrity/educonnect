class CreateImages < ActiveRecord::Migration[7.1]
  def change
    create_table :images do |t|
      t.string :value
      t.integer :priority
      t.references :lesson, null: false, foreign_key: true
      t.references :instructor, foreign_key: { to_table: :users }, null: false

      t.timestamps
    end
  end
end
