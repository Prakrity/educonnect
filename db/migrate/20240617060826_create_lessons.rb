class CreateLessons < ActiveRecord::Migration[7.1]
  def change
    create_table :lessons do |t|
      t.references :course, null: false, foreign_key: true
      t.references :instructor, foreign_key: { to_table: :users }, null: false
      t.string :title
      t.integer :priority


      t.timestamps
    end
  end
end
