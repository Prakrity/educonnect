class CreateDescriptions < ActiveRecord::Migration[7.1]
  def change
    create_table :descriptions do |t|
      t.integer :priority
      t.references :lesson, null: false, foreign_key: true
      t.references :instructor, foreign_key: { to_table: :users }, null: false
      
      t.timestamps
    end
  end
end
