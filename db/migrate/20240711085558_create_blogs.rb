class CreateBlogs < ActiveRecord::Migration[7.1]
  def change
    create_table :blogs do |t|
      t.references :instructor, foreign_key: { to_table: :users }, null: false
      t.string :title
      t.timestamps
    end
  end
end
