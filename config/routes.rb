Rails.application.routes.draw do

  devise_for :users, controllers: {
  sessions: 'users/sessions',
  registrations: 'users/registrations'
  }

  namespace :learners do
    resources :dashboard, only: [:index]
    resources :courses, only: [:show, :index]
    resources :lessons, only: [:show]
    resources :quizzes, only: [:show, :index] do
      collection do
        get 'level', to: 'quizzes#level'
        post '/:id', to: 'quizzes#submit_quiz_answer', as: 'submit_quiz_answer'
      end
    end
  end

  namespace :instructors do
    resources :dashboard, only: [:index]
    resources :courses
    resources :lessons do
      resources :sub_titles, only: [:create, :destroy, :update]
      resources :images, only: [:create, :destroy, :update]
      resources :descriptions, only: [:create, :destroy, :update]
    end
    resources :quizzes
    resources :blogs
  end

  # Define API routes for blogs
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :blogs, only: [:index, :show]
      # Add more API endpoints as needed
    end
  end

end