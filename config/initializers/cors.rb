Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins '*'  # Adjust as per your React app's domain
    resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end
