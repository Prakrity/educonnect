import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="alert"
export default class extends Controller {
    static targets = ["modal"];

    connect() {
      console.log("Modal controller connected");
    }
  
    openModal(event) {
      const modalId = event.currentTarget.getAttribute("data-modal-target");
      const modal = this.findModal(modalId);
  
      if (modal) {
        modal.classList.remove("hidden");
      }
    }
  
    closeModal(event) {
      const modalId = event.currentTarget.getAttribute("data-modal-target");
      const modal = this.findModal(modalId);
  
      if (modal) {
        modal.classList.add("hidden");
      }
    }
  
    findModal(modalId) {
      return document.getElementById(modalId);
    }
  }
  