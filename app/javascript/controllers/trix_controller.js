import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    console.log("Trix editor connected!");
  
  }

  addImages(event) {
    const figures = this.element.querySelectorAll("figure[data-trix-attachment]");

    figures.forEach((figure) => {
      const attachmentData = JSON.parse(figure.getAttribute("data-trix-attachment"));
      const imgUrl = attachmentData.url || attachmentData.content; // Handle different attachment structures
      if (!figure.querySelector("img") && imgUrl) {
         img = document.createElement("img");
        img.src = imgUrl;
        img.className = "attachment--image";
        figure.insertBefore(img, figure.firstChild);
      }
    });
  }
}

