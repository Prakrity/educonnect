class Description < ApplicationRecord
  belongs_to :lesson
  belongs_to :instructor, class_name: 'User', foreign_key: 'instructor_id'

  has_rich_text :value
  
  # Validations
  validates :value, presence: true
  validates :priority, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :lesson_id, presence: true
  validates :instructor_id, presence: true

  def upcoming_priority
    lesson.combined_contents.collect(&:priority).max.to_i + 1
  end
end
