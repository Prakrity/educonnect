class Blog < ApplicationRecord
  belongs_to :instructor, class_name: 'User'

  validates :description, presence: true
  validates :title, presence: true, uniqueness: { scope: :instructor_id }
  validates :instructor_id, presence: true

  has_rich_text :description

  def trix_description
    description.body.to_s if description.present?
  end
end
