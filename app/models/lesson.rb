class Lesson < ApplicationRecord
  belongs_to :course
  belongs_to :instructor, class_name: 'User'
  has_many :sub_titles, -> { order(:priority) }, dependent: :destroy
  has_many :descriptions, -> { order(:priority) }, dependent: :destroy
  has_many :images, -> { order(:priority) }, dependent: :destroy
  has_many :quiz, dependent: :destroy

  validates :course_id, presence: true
  validates :title, presence: true, uniqueness: { scope: :course_id }
  validates :priority, presence: true, numericality: { only_integer: true, greater_than: 0 }, uniqueness: { scope: :course_id }
  validates :instructor_id, presence: true

  validate :priority_must_be_unique_within_course

  def combined_contents
    combined = []

    # Fetch all subtitles, descriptions, and images associated with this lesson
    subtitles = sub_titles.to_a.select(&:persisted?)
    descriptions = self.descriptions.to_a.select(&:persisted?)
    images = self.images.to_a.select(&:persisted?)

    combined += subtitles
    combined += descriptions
    combined += images

    # Sort combined array by priority
    combined.sort_by! { |item| item.priority }

    combined
  end

  private

  def priority_must_be_unique_within_course
    if course.lessons.where(priority: priority).exists? && (new_record? || changed.include?('priority'))
      errors.add(:priority, "must be unique within the course")
    end
  end
end
