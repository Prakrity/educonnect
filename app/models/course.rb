class Course < ApplicationRecord
  belongs_to :instructor, class_name: 'User'
  has_many :lessons, dependent: :destroy
  has_many :users
  has_many :quizzes, through: :lessons

  validates :title, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 1000 }
  validates :instructor_id, presence: true
end
