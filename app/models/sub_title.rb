class SubTitle < ApplicationRecord
    belongs_to :lesson
    belongs_to :instructor, class_name: 'User', foreign_key: 'instructor_id'  # Assuming instructor_id is a foreign key in sub_titles table

    validates :value, presence: true
    validates :priority, numericality: { only_integer: true }
    validates :instructor_id, presence: true

    def upcoming_priority
        lesson.combined_contents.collect(&:priority).max.to_i + 1  
    end
end
