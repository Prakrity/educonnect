# app/models/quiz.rb
class Quiz < ApplicationRecord
  belongs_to :lesson
  validates :lesson_id, presence: true
  has_one :course, through: :lesson
  

  def self.check_answer(params)
    @feedback = {}
    params['quiz_ids'].each do |quiz|
      quiz_id = Quiz.find_by_id(quiz)
      if params['quiz_answers'][quiz] == quiz_id.right_answer
        return true
      else false
      end
    end
  end
end
