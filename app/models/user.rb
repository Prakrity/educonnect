class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :courses, foreign_key: :instructor_id, dependent: :destroy
  has_many :lessons, foreign_key: :instructor_id, dependent: :destroy
  has_many :blogs, foreign_key: :instructor_id, dependent: :destroy
  has_many :quizzes, foreign_key: :instructor_id, dependent: :destroy
  serialize :role, JSON

  # Ensure the role column contains an array
  before_save :ensure_role_is_array

  private

  def ensure_role_is_array
    self.role = [] if self.role.nil?
  end
end
