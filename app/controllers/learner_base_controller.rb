class LearnerBaseController < ApplicationController
  layout 'learners'
  before_action :authenticate_user!
  before_action :ensure_learner
  before_action :current_learner

  private

  def current_learner
    @current_learner = current_user
  end

  def ensure_learner
    unless session[:login_as] == 'learner'
      flash[:alert] = "You are not authorized to access this page."
      redirect_to instructors_dashboard_index_path # or any other path you want to redirect to
    end
  end
end
