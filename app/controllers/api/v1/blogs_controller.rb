# app/controllers/api/v1/blogs_controller.rb
module Api
  module V1
    class BlogsController < ApplicationController
      before_action :set_blog, only: [:show]

      # GET /api/v1/blogs
      def index
        @blogs = Blog.all
        render json: @blogs.as_json(only: [:id, :title, :created_at], methods: [:trix_description])
      end

      # GET /api/v1/blogs/:id
      def show
        render json: @blog.as_json(methods: [:trix_description])
      end

      private

      def set_blog
        @blog = Blog.find(params[:id])
      end
    end
  end
end
