# app/controllers/users/registrations_controller.rb
class Users::RegistrationsController < Devise::RegistrationsController
  before_action :check_role, only: [:create]

  private

  def check_role
    # Assume role is an array of roles
    if params[:user][:role] && !params[:user][:role].include?(params[:login_as])
      flash[:alert] = "You are not authorized to sign up as #{params[:login_as]}, Set role as #{params[:login_as]} and try again."
      redirect_to new_user_registration_path
    end
  end

  def sign_up_params
    params.require(:user).permit(:name, { role: [] }, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:name, { role: [] }, :email, :password, :password_confirmation, :current_password)
  end
end
