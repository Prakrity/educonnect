# app/controllers/users/sessions_controller.rb
class Users::SessionsController < Devise::SessionsController
  before_action :check_role, only: [:create]

  def destroy
    super do |resource|
      session[:login_as] = nil
    end
  end

  private

  def check_role
    user = User.find_by(email: params[:user][:email])
    if user
      unless user.role.include?(params[:login_as])
        flash[:alert] = "You are not authorized to log in as #{params[:login_as]}."
        redirect_to new_user_session_path
      end
    end
  end

end
