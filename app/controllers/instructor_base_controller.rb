class InstructorBaseController < ApplicationController
  layout 'instructors'
  before_action :authenticate_user!
  before_action :ensure_instructor
  before_action :current_instructor

  private

  def current_instructor
    @current_instructor = current_user
  end

  def ensure_instructor
    unless session[:login_as] == 'instructor'
      flash[:alert] = "You are not authorized to access this page."
      redirect_to learners_dashboard_index_path # or any other path you want to redirect to
    end
  end
end
