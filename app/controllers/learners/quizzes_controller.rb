class Learners::QuizzesController < LearnerBaseController
  before_action :authenticate_user!
  before_action :set_course, only: [:show, :level, :submit_quiz_answer]
  before_action :set_quizzes, only: :submit_quiz_answer

  def index
    @courses = Course.all
  end

  def show
    if params[:level].present?
      @quizzes = @course.quizzes.where(level: params[:level]).shuffle
      @quiz_results = {}
    end
  end

  def level; end

  def submit_quiz_answer
    if missing_answers?
      set_flash_alert_and_redirect
      return
    end

    process_quiz_answers
    calculate_quiz_summary

    render :show
  end

  private

  def set_course
    @course = Course.find(params[:id] || params[:course_id])
  end

  def set_quizzes
    @quizzes = Quiz.find(params[:quiz_ids])
  end

  def missing_answers?
    !params[:quiz_answers] || @quizzes.count != params[:quiz_answers].keys.count
  end

  def set_flash_alert_and_redirect
    flash[:alert] = "Please answer all questions."
    redirect_to learners_quiz_path(@course.id, level: params[:level])
  end

  def process_quiz_answers
    @quiz_results = {}
    @correct_count = 0

    @quizzes.each do |quiz|
      user_answer = params[:quiz_answers][quiz.id.to_s]
      correct_answer = quiz.right_answer

      if correct_answer == user_answer
        @quiz_results[quiz.id.to_s] = [true, "Right Answer", user_answer]
        @correct_count += 1
      else
        @quiz_results[quiz.id.to_s] = [false, "Wrong Answer, Right is #{correct_answer}", user_answer]
      end
    end
  end

  def calculate_quiz_summary
    total_questions = @quizzes.size
    wrong_count = total_questions - @correct_count
    percentage = (@correct_count.to_f / total_questions * 100).round(2)

    @quiz_summary = {
      total_questions: total_questions,
      correct_count: @correct_count,
      wrong_count: wrong_count,
      percentage: percentage
    }
  end
end
