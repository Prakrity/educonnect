class Learners::LessonsController < LearnerBaseController
  before_action :authenticate_user!
  before_action :set_course_and_lesson, only: [:show]

  def show
    @combined_contents = @lesson.combined_contents
    set_navigation_lessons
    @lessons = Lesson.all
  end

  private

  def set_course_and_lesson
    @lesson = Lesson.find_by_id(params[:id])
    @course = @lesson.course
  end

  def set_navigation_lessons
    lessons = @course.lessons.order(priority: :asc)
    current_lesson_index = lessons.index(@lesson)
    
    @previous_lesson = lessons[current_lesson_index - 1] if current_lesson_index > 0
    @next_lesson = lessons[current_lesson_index + 1] if current_lesson_index < lessons.size - 1
  end
end
