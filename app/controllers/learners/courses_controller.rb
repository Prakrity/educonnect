class Learners::CoursesController < LearnerBaseController
    before_action :authenticate_user!

    def index
        @courses = Course.all
    end

    def show
        @course = Course.find_by_id(params[:id])
    end
end