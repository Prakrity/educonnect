# app/controllers/application_controller.rb
class ApplicationController < ActionController::Base
  protected

  def after_sign_in_path_for(resource)
    session[:login_as] = params[:login_as]
    case session[:login_as]
    when 'learner'
      learners_dashboard_index_path
    when 'instructor'
      instructors_dashboard_index_path
    else
      learners_dashboard_index_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end
end
