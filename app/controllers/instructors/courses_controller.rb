# app/controllers/instructors/courses_controller.rb
class Instructors::CoursesController < InstructorBaseController
  before_action :authenticate_user!
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  def index
    @courses = current_instructor.courses
  end

  def show
  end

  def new
    @course = current_instructor.courses.build
  end

  def edit
  end

  def create
    @course = current_instructor.courses.build(course_params)

    if @course.save
      redirect_to instructors_course_path(@course), notice: 'Course was successfully created.'
    else
      flash.now[:alert] = 'There was an error creating the course.'
      render :new
    end
  end

  def update
    if @course.update(course_params)
      redirect_to instructors_course_path(@course), notice: 'Course was successfully updated.'
    else
      flash.now[:alert] = 'There was an error updating the course.'
      render :edit
    end
  end

  def destroy
    @course.destroy
    redirect_to instructors_courses_path, notice: 'Course was successfully destroyed.'
  end

  private

  def set_course
    @course = current_instructor.courses.find(params[:id])
  end

  def course_params
    params.require(:course).permit(:title, :description)
  end
end

  