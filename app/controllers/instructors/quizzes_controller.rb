class Instructors::QuizzesController < InstructorBaseController
    before_action :authenticate_user!
    before_action :set_quiz, only: [:edit, :update, :destroy]
  
    def index
      @courses = current_instructor.courses
    end
  
    def show
      @course = Course.find_by_id(params[:id])
      @quizzes = @course.quizzes
    end
  
    def new
      @quiz = current_instructor.quizzes.build
    end
  
    def edit
    end
  
    def create
      @quiz = current_instructor.quizzes.build(quiz_params)
        
      if @quiz.save
        redirect_to instructors_quiz_path(@quiz.course), notice: 'Quiz was successfully created.'
      else
        flash.now[:alert] = 'There was an error creating the quiz.'
        render :new
      end
    end
  
    def update
      if @quiz.update(quiz_params)
        redirect_to instructors_quiz_path(@quiz.course), notice: 'Quiz was successfully updated.'
      else
        flash.now[:alert] = 'There was an error updating the quiz.'
        render :edit
      end
    end
  
    def destroy
      @quiz.destroy
      redirect_to instructors_quizzes_path, notice: 'Quiz was successfully destroyed.'
    end
  
    private
  
    def set_quiz
      @quiz = current_instructor.quizzes.find(params[:id])
    end
  
    def quiz_params
      params.require(:quiz).permit(:question, :lesson_id, :answer1, :answer2, :answer3, :answer4, :right_answer, :level)
    end
  end
  