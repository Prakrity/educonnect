class Instructors::LessonsController < InstructorBaseController
  before_action :authenticate_user!
  before_action :set_lesson, only: [:show, :edit, :update, :destroy]

  def index
    @lessons = current_instructor.lessons
  end

  def show
    @sub_title = @lesson.sub_titles.build
    @description = @lesson.descriptions.build
    @image = @lesson.images.build
    @combined_contents = @lesson.combined_contents
  end

  def new
    @lesson = current_instructor.lessons.build
  end

  def edit
  end

  def create
    @lesson = current_instructor.lessons.build(lesson_params)

    if @lesson.save
      redirect_to instructors_lesson_path(@lesson), notice: 'Lesson was successfully created.'
    else
      flash.now[:alert] = 'There was an error creating the lesson.'
      render :new
    end
  end

  def update
    if @lesson.update(lesson_params)
      redirect_to instructors_lesson_path(@lesson), notice: 'Lesson was successfully updated.'
    else
      flash.now[:alert] = 'There was an error updating the lesson.'
      render :edit
    end
  end

  def destroy
    @lesson.destroy
    redirect_to instructors_lessons_path, notice: 'Lesson was successfully destroyed.'
  end

  def preview
  end


  private

  def set_lesson
    @lesson = current_instructor.lessons.find(params[:id])
  end

  def lesson_params
    params.require(:lesson).permit(:title, :priority, :course_id)
  end
end
