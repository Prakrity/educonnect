module Instructors
  class SubTitlesController < InstructorBaseController
    before_action :authenticate_user!
    before_action :set_lesson
    before_action :authorize_instructor
    before_action :set_sub_title, only: [:update, :destroy]

    def create
      @sub_title = @lesson.sub_titles.new(sub_title_params)
      @sub_title.instructor_id = current_user.id  # Assign the current user's ID as instructor_id
      if @sub_title.save
        redirect_to instructors_lesson_path(@lesson), notice: 'Subtitle was successfully created.'
      else
        redirect_to instructors_lesson_path(@lesson)
      end
    end

    def update
      if @sub_title.update(sub_title_params)
        redirect_to instructors_lesson_path(@lesson), notice: 'Subtitle was successfully updated.'
      else
        render 'instructors/lessons/show'
      end
    end

    def destroy
      @sub_title.destroy
      redirect_to instructors_lesson_path(@lesson), notice: 'Subtitle was successfully deleted.'
    end

    private

    def set_lesson
      @lesson = Lesson.find(params[:lesson_id])
    end

    def authorize_instructor
      unless @lesson.course.instructor_id == current_user.id
        redirect_to instructors_lessons_path, alert: 'You are not authorized to perform this action.'
      end
    end

    def set_sub_title
      @sub_title = @lesson.sub_titles.find(params[:id])
    end

    def sub_title_params
      params.require(:sub_title).permit(:value, :priority)
    end
  end
end
