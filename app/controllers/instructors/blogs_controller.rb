class Instructors::BlogsController < InstructorBaseController
  before_action :authenticate_user!
  before_action :set_blog, only: [:show, :edit, :update, :destroy]

  def index
    @blogs = current_instructor.blogs
  end

  def show
  end

  def new
    @blog = current_instructor.blogs.build
  end

  def edit
  end

  def create
    @blog = current_instructor.blogs.build(blog_params)
    puts "==================#{@blog.inspect}"
    puts "==================#{@blog.save}"
    if @blog.save
      redirect_to instructors_blog_path(@blog), notice: 'blog was successfully created.'
    else
      flash.now[:alert] = 'There was an error creating the blog.'
      render :new
    end
  end

  def update
    if @blog.update(blog_params)
      redirect_to instructors_blog_path(@blog), notice: 'blog was successfully updated.'
    else
      flash.now[:alert] = 'There was an error updating the blog.'
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to instructors_blogs_path, notice: 'blog was successfully destroyed.'
  end

  def preview
  end


  private

  def set_blog
    @blog = current_instructor.blogs.find(params[:id])
  end

  def blog_params
    params.require(:blog).permit(:title, :description)
  end
end
