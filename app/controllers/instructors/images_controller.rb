module Instructors
  class ImagesController < InstructorBaseController
    before_action :authenticate_user!
    before_action :set_lesson
    before_action :authorize_instructor
    before_action :set_image, only: [:update, :destroy]

    def create
      @image = @lesson.images.new(image_params)
      @image.instructor_id = current_user.id  # Assign the current user's ID as instructor_id
      if @image.save
        redirect_to instructors_lesson_path(@lesson), notice: 'Image was successfully created.'
      else
        redirect_to instructors_lesson_path(@lesson)
      end
    end

    def update
      if @image.update(image_params)
        redirect_to instructors_lesson_path(@lesson), notice: 'Image was successfully updated.'
      else
        render 'instructors/lessons/show'
      end
    end

    def destroy
      @image.destroy
      redirect_to instructors_lesson_path(@lesson), notice: 'Image was successfully deleted.'
    end

    private

    def set_lesson
      @lesson = Lesson.find(params[:lesson_id])
    end

    def authorize_instructor
      unless @lesson.course.instructor_id == current_user.id
        redirect_to instructors_lessons_path, alert: 'You are not authorized to perform this action.'
      end
    end

    def set_image
      @image = @lesson.images.find(params[:id])
    end

    def image_params
      params.require(:image).permit(:value, :priority)
    end
  end
end
