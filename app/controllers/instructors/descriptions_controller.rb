module Instructors
  class DescriptionsController < InstructorBaseController
    before_action :authenticate_user!
    before_action :set_lesson
    before_action :authorize_instructor
    before_action :set_description, only: [:update, :destroy]

    def create
      @description = @lesson.descriptions.new(description_params)
      @description.instructor_id = current_user.id  # Assign the current user's ID as instructor_id
      if @description.save
        redirect_to instructors_lesson_path(@lesson), notice: 'Description was successfully created.'
      else
        redirect_to instructors_lesson_path(@lesson)
      end
    end

    def update
      if @description.update(description_params)
        redirect_to instructors_lesson_path(@lesson), notice: 'Description was successfully updated.'
      else
        render 'instructors/lessons/show'
      end
    end

    def destroy
      @description.destroy
      redirect_to instructors_lesson_path(@lesson), notice: 'Description was successfully deleted.'
    end

    private

    def set_lesson
      @lesson = Lesson.find(params[:lesson_id])
    end

    def authorize_instructor
      unless @lesson.course.instructor_id == current_user.id
        redirect_to instructors_lessons_path, alert: 'You are not authorized to perform this action.'
      end
    end

    def set_description
      @description = @lesson.descriptions.find(params[:id])
    end

    def description_params
      params.require(:description).permit(:value, :priority)
    end
  end
end
